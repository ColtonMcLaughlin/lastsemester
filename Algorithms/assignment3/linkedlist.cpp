#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

using namespace std;

struct nodetype
{
	int id;
	string first;
	string last;
	string department;
	double salary;
	nodetype* link;
};

void createlist(nodetype*& head, nodetype*& tail)
{
	head = new nodetype;
	tail = new nodetype;
	head->id = -1;
	tail->id = 9999;
	head->link = tail;
	tail->link = NULL;
}

bool emptylist(nodetype* head, nodetype* tail)
{
	return head->link == tail;
}

void traversem(nodetype* head, nodetype* tail, ofstream& outfile)
{
	nodetype* c;
	if (!emptylist(head, tail))
	{
		outfile << left << setw(5) << "ID" << setw(15) << "Name" << setw(13) << "Department"
			<< setw(8) << "Salary" << endl;
		c = head->link;
		while (c != tail)
		{
			string fullname;
			fullname = c->first + " " + c->last;
			outfile << setw(5) << c->id << setw(15) << fullname << setw(13) << c->department <<
				setw(10) << c->salary << endl;
			c = c->link;
		}
	}
	else
		outfile << "List is Empty\n";
	outfile << endl;
}

void insertlist(nodetype* head, nodetype* tail,int id,string first,string last,
	string department, double salary)
{
	nodetype* knew, * prior, * next;
	knew = new nodetype;
	knew->id = id;
	knew->first = first;
	knew->last = last;
	knew->department = department;
	knew->salary = salary;

	prior = head;
	next = head->link;
	while ((next != tail) && (knew->id > next->id))
	{
		prior = next;
		next = next->link;
	}
	prior->link = knew;
	knew->link = next;
}

void readem(nodetype* head, nodetype* tail)
{
	int id;
	string first, last, department;
	double salary;

	ifstream infile("Data.txt");
	while (!infile.eof())
	{
		infile >> id >> first >> last >> department >> salary;
		insertlist(head, tail, id, first, last, department, salary);
	}
}

void deletelist(nodetype* head, nodetype* tail, int id, string first, string last,
	string department, double salary)
{
	nodetype* c, *prior, *next;
	prior = head;
	c = head->link;
	next = c->link;

	while ((next != tail) && (id > c->id))
	{
		prior = c;
		c = next;
		next = next->link;
	}

	if (c->id == id)
	{
		prior->link = next;
		delete c;
	}
	else
		cout << "id not in list\n";
}

int main()
{
	ofstream outfile("Sample.txt");
	nodetype* head, * tail;

	createlist(head, tail);
	readem(head, tail);
	traversem(head, tail, outfile);
	insertlist(head, tail, 7676, "Charles", "Darwin", "Biology", 50000);
	deletelist(head, tail, 3966, "Old", "McDonald", "Agriculture", 75000);
	traversem(head, tail, outfile);

	system("pause");
	return 0;
}



