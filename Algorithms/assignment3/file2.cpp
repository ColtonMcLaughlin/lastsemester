#include <iostream>
#include <fstream>
#include <iomanip>


using namespace std;

struct hashNode
{
	string word;
    int count = 0;
	hashNode* link = NULL;
};

struct heapNode{
	string word;
	int count = 0;
};

void createlist(hashNode*& head)
{
	head = new hashNode;
	head->link = NULL;
}

bool emptylist(hashNode* head)
{
	return head->link == NULL;
}

void traverseList(hashNode* head, int i, ofstream& outf){
    outf << "Traversal "<< i << ":" << endl;
	hashNode* c;
	if (!emptylist(head))
	{
		c = head->link;
		while (c != NULL)
		{
			outf << c->word << " " << c->count << endl;
			c = c->link;
		}
	}
	else
		outf << "List is Empty\n\n";
}

void updatelist(hashNode* head, string word, int& uniqueWords)
{
	hashNode* knew, * prior, * next;
	knew = new hashNode;
	knew->word = word;
	knew->count++;

	prior = head;
	next = head->link;

	while ((next != NULL) && knew->word != next->word){
		prior = next;
		next = next->link;
	}

	if(next != NULL && knew->word == next->word){
		next->count++;
        delete knew;
	}else {
		prior->link = knew;
		knew->link = next;
		uniqueWords++;
	}
}

void deleteList(hashNode* head)
{
   struct hashNode* tmp;

   while (head != NULL)
    {
       tmp = head;
       head = head->link;
       delete tmp;
    }
}

int getKey(string word, int hashTableLength){
	
	int num = int(word[0]);
	for (int i = 1; i < word.length(); i++){
		num *= int(word[i]);
	}
	return abs(num % hashTableLength);

	// starter hash function
	// int index = (int)word[0];
	// return abs(index % size);
}

int findWord(string word, int hashTableLength, hashNode** hashtable){
	int occurances = 0;
	hashNode* head = hashtable[getKey(word, hashTableLength)];
	hashNode* c;
	if (!emptylist(head))
	{
		c = head->link;
		while (c != NULL)
		{
			if(c->word == word){
				occurances = c->count;
			}
			c = c->link;
		}
	}
	else
		cout << "List is Empty\n\n";
	return occurances;
}

int readFile(string file, int hashTableLength, hashNode** hashtable, int& uniqueWords, int& sentenceCount){
    
    ifstream inf(file);
	int wordCount = 0;
    while (!inf.eof()){
        string line = "";
		string word = "";
        getline(inf, line);

        for (int i = 0, len = line.size(); i < len; i++)
        {	
			if (line[i] == '.'){
				sentenceCount++;
			}

            if (ispunct(line[i]))
            {
                line.erase(i--, 1);
                len = line.size();
            }
        }

		for (int i = 0; i < line.length(); i++){
			line[i] = tolower(line[i]);
		}

		stringstream ss(line);
		while(ss >> word){
			updatelist(hashtable[getKey(word, hashTableLength)], word, uniqueWords);
			wordCount++;
		}

    }
	return wordCount;
}

int valuesToHeap(hashNode* head, int i, heapNode* heap, int heapLocation){
	hashNode* c;
	if (!emptylist(head))
	{
		c = head->link;
		while (c != NULL)
		{
			heap[heapLocation].word = c->word;
			heap[heapLocation].count = c->count;
			c = c->link;
			heapLocation++;
		}
	}

	return heapLocation;
}

void heapify(heapNode arr[], int n, int i){
    int largest = i;
    int l = 2 * i + 1;
    int r = 2 * i + 2;
    if (l < n && arr[l].count > arr[largest].count)
        largest = l;
    if (r < n && arr[r].count > arr[largest].count)
        largest = r;
    if (largest != i) {
        swap(arr[i], arr[largest]);
        heapify(arr, n, largest);
    }
}

void heapSort(heapNode arr[], int n){
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);
    for (int i = n - 1; i >= 0; i--){
        swap(arr[0], arr[i]);
        heapify(arr, i, 0);
    }
}

void bottom150(heapNode arr[], int n, ofstream& outf){
	outf << "\nBottom 150\n";
	if (n > 150){
		for (int i = 0; i < 150; i++){
			outf << arr[i].word << " " << arr[i].count << endl;
		}
	}else{
		for (int i = 0; i < n; i++)
			outf << arr[i].word << " " << arr[i].count << endl;
		}
	outf << endl;
}

void top150(heapNode arr[], int n, ofstream& outf){
	outf << "\nTop 150\n";
	if (n > 150){
		for (int i = n - 1; i > n - 150; i--){
			outf << arr[i].word << " " << arr[i].count << endl;
		}
	}else{
		for (int i = n; i > -1; i--)
			outf << arr[i].word << " " << arr[i].count << endl;
		}
	outf << endl;
}

int main(int argc, char* argv[]){
	const int hashTableLength = 10000;
	hashNode* hashtable[hashTableLength];
	int uniqueWords = 0;
	int sentenceCount = 0;
    cout << argv[1] << " " << argv[2] << endl;
	for(int i = 0; i < hashTableLength; i++){
		createlist(hashtable[i]);
	}

	// ofstream outf("Hashresults.txt");
	const int wordCount = readFile(argv[1], hashTableLength, hashtable, uniqueWords, sentenceCount);

	// for(int i = 0; i < hashTableLength; i++) {
	// 	traverseList(hashtable[i], i, outf);
	// }
	
	// outf.close();

	heapNode heap[uniqueWords];
	
	int heapLocation = 0;
	for(int i = 0; i < hashTableLength; i++) {
		heapLocation = valuesToHeap(hashtable[i], i, heap, heapLocation);
	}
	
	int n = sizeof(heap) / sizeof(heap[0]);
	heapSort(heap, n);
    ofstream outf(argv[2]);
	
	for(int i = 0; i < uniqueWords; i++){
		outf << heap[i].word << ": " << heap[i].count << endl;
	}

	// memory cleanup
	for(int i = 0; i < hashTableLength; i++) {
		deleteList(hashtable[i]);
	}

    return 0;
}