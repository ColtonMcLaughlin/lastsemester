import cv2
#read image in black and white
img = cv2.imread('/home/colton/projects/lastsemester/Algorithms/presentation/banana.jpg', cv2.IMREAD_GRAYSCALE)
# img = cv2.imread('/home/colton/projects/lastsemester/Algorithms/presentation/bananas.jpg', cv2.IMREAD_GRAYSCALE)

# create a black and white version of the original image
cv2.imwrite("blackAndWhite.jpg", img)

# print results of reading original file
with open('originalFile.txt', 'w') as f:
    for row in range(img.shape[0]):
        for column in range(img.shape[1]):
            f.write("{:<5}".format(str(img[row][column])))
        f.write('\n')
f.close()

# change values for original file
# 255 is the whitest value and 0 is the blackest
for row in range(img.shape[0]):
    for column in range(img.shape[1]):
        if img[row][column] > 50 and img[row][column] < 245 :
            img[row][column] = 255
        else: 
            img[row][column] = 0


# change values for second file
# for row in range(img.shape[0]):
#     for column in range(img.shape[1]):
#         if img[row][column] > 120 and img[row][column] < 155 :
#             img[row][column] = 255
#         else: 
#             img[row][column] = 0

# print changed output of file
with open('changedFile.txt', 'w') as f:
    for row in range(img.shape[0]):
            for column in range(img.shape[1]):
                f.write("{:<5}".format(str(img[row][column])))
            f.write('\n')    
f.close()

# create a new image with the changed values
cv2.imwrite("segmentedImage5.jpg", img)