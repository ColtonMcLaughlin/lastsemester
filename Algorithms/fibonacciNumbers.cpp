// https://en.wikipedia.org/wiki/C_data_types
// https://miniwebtool.com/list-of-fibonacci-numbers/?number=100
#include <iostream>
#include <iomanip>

using namespace std;

int fib1(int n){
    if(n == 0) return 0;
    if(n == 1) return 1;
    return fib1(n - 1) + fib1(n - 2);
}

int fib2(int n){
    //good only to 46th number
    if (n == 0) return 0;
    if (n == 1) return 1;

    int arr[n + 1];
    arr[0] = 0; 
    arr[1] = 1;

    for(int i = 2; i <= n; i++){
        arr[i] = arr[i - 1] + arr[i - 2];
    }

    // cout << "BEGIN OF INTERNAL ARRAY" << endl;
    // for(int i = 0; i <= n; i++){
    //     cout << i << ": " << arr[i] << endl;
    // }
    // cout << "END OF INTERNAL ARRAY" << endl;

    return arr[n];
}

unsigned long long int fib3(int n){
    //good to 93
    if (n == 0) return 0;
    if (n == 1) return 1;

    unsigned long long int arr[n + 1];
    arr[0] = 0; 
    arr[1] = 1;

    for(int i = 2; i <= n; i++){
        arr[i] = arr[i - 1] + arr[i - 2];
    }

    cout << "BEGIN OF INTERNAL ARRAY" << endl;
    for(int i = 0; i <= n; i++){
        cout << fixed << setprecision(0) << i << ": " << arr[i] << endl;
    }
    cout << "END OF INTERNAL ARRAY" << endl;

    return arr[n];
}

int main(){
    cout << "fib1\n";
    int result = fib1(25);
    cout << "Result: "<< result << endl;
    
    cout << "fib2\n";
    int result2 = fib2(46);
    cout << "Result: " << result2 << endl;

    cout << "fib3\n";
    unsigned long long int result3 = fib3(93);
    cout << "Result: " << result3 << endl;

    return 0;
}