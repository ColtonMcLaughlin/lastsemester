#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

vector<vector<int>> read(string fileName){
    vector<vector<int>>adjMatrix;
    ifstream inf(fileName);
    string num;
    int row = 0;
    int column = 0;
    while(!inf.eof()){
        string line = "";
        getline(inf, line);
        for (int i = 0, len = line.size(); i < len; i++)
        {	
            if (ispunct(line[i]))
            {
                line.erase(i--, 1);
                len = line.size();
            }
        }
        stringstream ss(line);
        vector<int>nums;
        while(ss >> num){
            nums.push_back(stoi(num));
        }
        adjMatrix.push_back(nums);
        row++;
    }
    return adjMatrix;
}

void printAdjMatrix(vector<vector<int>> adjMatrix){
    for (int i = 0; i < adjMatrix.size(); i++){
        for (int j = 0; j < adjMatrix[i].size(); j++){
            cout << adjMatrix[i][j] << " ";
        }
        cout << endl;
    }
}

int main(){

    vector<vector<int>> adjMatrix = read("Adjacency matrix.txt");
    printAdjMatrix(adjMatrix);
    
    return 0;
}