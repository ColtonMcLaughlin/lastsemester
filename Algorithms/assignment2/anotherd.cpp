#include <limits.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sstream>

using namespace std;

struct edgetype {
	string v1, v2;
	int v1_number, v2_number;
	int wt;
};

struct qtype {
	string v1, v2;
	int v1_number, v2_number;
	int wt;
	qtype* link;
};

void createq(qtype*& qhead, qtype*& qtail) {
	qhead = new qtype;
	qtail = new qtype;
	qhead->link = qtail;
	qtail->link = NULL;
}

bool emptyq(qtype* qhead, qtype* qtail) {
	return qhead->link == qtail;
}

edgetype deq(qtype* qhead, qtype* qtail) {
	edgetype u;
	qtype* c;
	c = qhead->link;
	qhead->link = c->link;
	u.v1 = c->v1;
	u.v2 = c->v2;
	u.v1_number = c->v1_number;
	u.v2_number = c->v2_number;
	u.wt = c->wt;
	delete c;
	return u;
}

void enq(qtype* qhead, qtype* qtail, string v1, string v2, int v1_number, int v2_number, int wt) {
	qtype* knew, * next, * previous;
	knew = new qtype;
	knew->v1 = v1;
	knew->v2 = v2;
	knew->v1_number = v1_number;
	knew->v2_number = v2_number;
	knew->wt = wt;
	previous = qhead;
	next = qhead->link;
	while (next != qtail && wt >= next->wt) {
		previous = next;
		next = next->link;
	}
	previous->link = knew;
	knew->link = next;
}

int nthSubstr(int n, const string& s, const string& p) {
   string::size_type i = s.find(p);     // Find the first occurrence

   int j;
   for (j = 1; j < n && i != string::npos; ++j)
      i = s.find(p, i+1); // Find the next occurrence

   if (j == n)
     return(i);
   else
     return(-1);
}

const int valuequeue(qtype* qhead, qtype* qtail) {
	ifstream inf("distances.txt");
	ofstream outf("valuequeue.txt");
	// numcount counts total cities found
	int numcount;
	string cities = "";
	int city = -1;
	string current_city_name, previous_city_name;
	while(!inf.eof()){
		string line = "";
		getline(inf, line);
		if(line[0] == '*') continue;
		if(isdigit(line[0]) == false){
			city++;
			if(city == 1){
				enq(qhead, qtail, current_city_name, current_city_name, 0, 0, 0);
			}
			numcount = 0;
			if (city == 0 || city == 1){
				cities = current_city_name;
			}else{
				cities += "," + current_city_name;
			}
			current_city_name = line.substr(0,line.find(','));
		}
		if(isdigit(line[0]) == true){
			stringstream ss(line);
    		string num;
			// counter tracks the number of distances found for each city
			unsigned int counter = 0;
			istringstream ss1(cities);
			string city_name;

			while(getline(ss1, city_name, ',')) {
				counter++;
			}
    		while (ss >> num) {
			string second_city = cities.substr(nthSubstr(numcount,cities,",") + 1,nthSubstr(numcount + 1,cities,",") - nthSubstr(numcount,cities,",") - 1);

			enq(qhead, qtail, current_city_name, second_city, city, numcount, stoi(num));
			
			numcount++;
    		}
		}
	}
	return numcount;
}

string* new_matrix(int **arr, int n, int m,qtype* qhead, qtype* qtail)
{
	string * cities = new string[n];

	ofstream outf("newadjmatrix.txt");
    
	while(!emptyq(qhead, qtail)){
		edgetype e = deq(qhead, qtail);

		arr[e.v1_number][e.v2_number] = e.wt;
		arr[e.v2_number][e.v1_number] = e.wt;
		cities[e.v1_number] = e.v1;
	}
	
	int longest_name = 0;
	for(int i = 0; i < m; i++){
		if(cities[i].length() > longest_name){
			longest_name = cities[i].length();
		}
	}

	//top labels
	outf << setw(longest_name + 1) << "";
	for (int j = 0; j < m; j++) {
		outf << setw(cities[j].length() + 1) << left << cities[j];
	}
	outf << endl;
	
	for (int i = 0; i < n; i++) {
		//side labels
        outf  << setw(longest_name + 1) << cities[i];
		//contents
		for (int j = 0; j < m; j++) {
			outf << setw(cities[j].length() + 1) << arr[i][j];
		}
		outf << endl;
	}
	outf << endl;
	outf.close();
	return cities;
}

int minDistance(int dist[], bool sptSet[], const int x)
{
	// Initialize min value
	int min = INT_MAX, min_index;

	for (int v = 0; v < x; v++)
		if (sptSet[v] == false &&
				dist[v] <= min)
			min = dist[v], min_index = v;

	return min_index;
}

// Function to print shortest
// path from source to j
// using parent array
void printPath(int parent[], int j, string *cities, int dist[])
{
		
	// Base Case : If j is source
	if (parent[j] == - 1)
		return;
	
	printPath(parent, parent[j], cities, dist);
	
	cout << ", " << cities[j] << " " << dist[j];
}

// A utility function to print
// the constructed distance
// array
void printSolution(int dist[], const int x, int parent[], string * cities)
{
	int src = 0;
	cout << left << setw(cities[src].length() + 4) << "Origin:" << setw(20) << "Destination:" << setw(15) << "Distance:" << "Route" << endl;
	// print distance to all cities
	// for (int i = 1; i <= x; i++)
	// {
	// 	// printf("\n%d -> %d \t\t %d\t\t%d ", src, i, dist[i], src);
	// 	cout << left << endl << setw(cities[src].length()) << cities[src] << " to " << setw(20) << cities[i] << setw(15) << dist[i] << cities[src] <<  " " << dist[src];
	// 	printPath(parent, i, cities, dist);
	// }

	//prints distance to this specified city
	int i = 127;

	cout << left << endl << setw(cities[src].length()) << cities[src] << " to " << setw(20) << cities[i] << setw(15) << dist[i] << cities[src] <<  " " << dist[src];
	printPath(parent, i, cities, dist);

}

// Function that implements Dijkstra's
// single source shortest path
// algorithm for a graph represented
// using adjacency matrix representation
void dijkstra(int **graph, int src, const int x, string * cities)
{
	
	// The output array. dist[i]
	// will hold the shortest
	// distance from src to i
	int dist[x];

	// sptSet[i] will true if vertex
	// i is included / in shortest
	// path tree or shortest distance
	// from src to i is finalized
	bool sptSet[x];

	// Parent array to store
	// shortest path tree
	int parent[x];

	// Initialize all distances as
	// INFINITE and stpSet[] as false
	for (int i = 0; i <= x; i++)
	{
		parent[0] = -1;
		dist[i] = INT_MAX;
		sptSet[i] = false;
	}

	// Distance of source vertex
	// from itself is always 0
	dist[src] = 0;

	// Find shortest path
	// for all vertices
	for (int count = 0; count <= x; count++)
	{
		// Pick the minimum distance
		// vertex from the set of
		// vertices not yet processed.
		// u is always equal to src
		// in first iteration.
		int u = minDistance(dist, sptSet, x);

		// Mark the picked vertex
		// as processed
		sptSet[u] = true;

		// Update dist value of the
		// adjacent vertices of the
		// picked vertex.
		for (int v = 0; v <= x; v++)

			// Update dist[v] only if is
			// not in sptSet, there is
			// an edge from u to v, and
			// total weight of path from
			// src to v through u is smaller
			// than current value of
			// dist[v]
			if (!sptSet[v] && graph[u][v] && dist[u] + graph[u][v] < dist[v])
			{
				parent[v] = u;
				dist[v] = dist[u] + graph[u][v];
			}
	}

	// print the constructed
	// distance array
	printSolution(dist, x, parent, cities);
}

int main()
{
	qtype *qhead, *qtail;
	createq(qhead, qtail);

	const int x = valuequeue(qhead, qtail);
	
	int n = x + 1;
	int m = x + 1;

    int **arr = new int*[x + 1];
	
    //zero out array
    for(int i = 0; i < n; i++)
    {
        arr[i]=new int[m];
        for(int j = 0; j < m; j++)
            arr[i][j] = 0;
    }

    string* cities = new_matrix(arr, n, m, qhead, qtail);  //This statement will pass the array as parameter.

	dijkstra(arr, 0, x, cities);
	cout << endl;

	 // array cleanup after use
    for(int i=0;i<n;i++)
        delete [] arr[i];
    delete [] arr;
	
	return 0;
}
