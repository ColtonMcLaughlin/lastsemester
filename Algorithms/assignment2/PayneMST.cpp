#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

const int maxv = 'M' + 1;
const int vertices = maxv - 'A';

void creategraph(int adj[][maxv]) {
	for (int i = 0; i < maxv; i++) {
		for (int j = 0; j < maxv; j++) {
			adj[i][j] = 0;
		}
	}
}

void readem(int adj[][maxv]) {
	ifstream inf("graphin.txt");
	char v1 = '0';
	char v2 = '0';
	int wt = 0;
	while (!inf.eof()) {
		inf >> v1 >> v2 >> wt;
		adj[v1][v2] = wt;
		adj[v2][v1] = wt;
	}
}

enum colortype { white, grey, black };

void initcolor(colortype color[]) {
	for (int i = 0; i < maxv; i++) {
		color[i] = white;
	}
}

struct edgetype {
	char v1, v2;
	int wt;
};

struct qtype {
	char v1, v2;
	int wt;
	qtype* link;
};

void printgraph(int adj[][maxv], ofstream& outf) {
	outf << " ";
	for (int j = 'A'; j < maxv; j++) {
		outf << setw(2) << static_cast<char>(j);
	}
	outf << endl;
	
	for (int i = 'A'; i < maxv; i++) {
		outf  << static_cast<char>(i) << " ";
		for (int j = 'A'; j < maxv; j++) {
			outf << adj[i][j] << " ";
		}
		outf << endl;
	}
	outf << endl;
}

void initAns(edgetype ans[]) {
	for (int i = 0; i < vertices; i++) {
		ans[i].v1 = '0';
		ans[i].v2 = '0';
		ans[i].wt = 0;
	}
}

void createq(qtype*& qhead, qtype*& qtail) {
	qhead = new qtype;
	qtail = new qtype;
	qhead->link = qtail;
	qtail->link = NULL;
}

bool emptyq(qtype* qhead, qtype* qtail) {
	return qhead->link == qtail;
}

edgetype deq(qtype* qhead, qtype* qtail) {
	edgetype u;
	qtype* c;
	c = qhead->link;
	qhead->link = c->link;
	u.v1 = c->v1;
	u.v2 = c->v2;
	u.wt = c->wt;
	delete c;
	return u;
}

void enq(qtype* qhead, qtype* qtail, char v1, char v2, int wt) {
	qtype* knew, * next, * previous;
	knew = new qtype;
	knew->v1 = v1;
	knew->v2 = v2;
	knew->wt = wt;
	previous = qhead;
	next = qhead->link;
	while (next != qtail && wt >= next->wt) {
		previous = next;
		next = next->link;
	}
	previous->link = knew;
	knew->link = next;
}

void algo(qtype *qhead, qtype *qtail, edgetype ans[], colortype color[], int adj[][maxv], char start) {
	int counter = 0;
	enq(qhead, qtail, start, '0', 0);
	while (!emptyq(qhead, qtail)) {
		edgetype e = deq(qhead,qtail);
		cout << e.v1 << " " << e.v2 << " " << e.wt << endl;
		if (e.wt != 0 && color[e.v1] != black) {
			ans[counter] = e;
			counter++;
			cout << "counter: " << counter << endl;

		}
		if (color[e.v1] != black) {
			for (char i = 'A'; i <= 'M'; i++) {
				if (adj[e.v1][i] != 0 && color[i] != black) {
					enq(qhead, qtail, i, e.v1, adj[e.v1][i]);
					color[i] = grey;
				}
			}
			color[e.v1] = black;
		}
	}
}

void printans(edgetype ans[], ofstream &outf) {
	int totalwt = 0;
	outf << "Minimum Spanning Tree\n";
	for (int i = 0; i < vertices; i++) {
		if (ans[i].wt != 0) {
			outf << "Edge " << i + 1 << endl;
			outf << "v1 " << ans[i].v1 << endl;
			outf << "v2 " << ans[i].v2 << endl;
			outf << "wt " << ans[i].wt << endl;
			outf << endl;
			totalwt += ans[i].wt;
		}
	}
	outf << "Total Weight " << totalwt;
	outf << endl;
}

int main() {
	ifstream inf("graphin.txt");
	ofstream outf("graphout.txt");

	int adj[maxv][maxv];

	creategraph(adj);
	readem(adj);
	printgraph(adj, outf);

	edgetype ans[vertices];
	initAns(ans);

	qtype *qhead, *qtail;
	createq(qhead, qtail);

	colortype color[maxv];
	initcolor(color);
	
	// for (int i = 0; i < maxv; i++){
	// 	cout << color[i] << " " << i << endl;
	// }

	algo(qhead, qtail, ans, color, adj, 'A');
	printans(ans, outf);

	// for (int i = 0; i < maxv; i++){
	// 	cout << color[i] << " " << i << endl;
	// }

	cout << "end: " << 77 - 'A' << endl;
	
	return 0;
	system("pause");
}