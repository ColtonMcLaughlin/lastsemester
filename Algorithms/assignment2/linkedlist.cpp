#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>

using namespace std;

const int maxlist = 50;

struct listtype
{
	int id;
	string first;
	string last;
	string department;
	double salary;
};

const listtype initrec
{
	0,"first", "last", "department", 0.0
};

bool emptylist(int& numlist)
{
	return numlist <= 0;
}

bool fulllist(int& numlist)
{
	return numlist >= maxlist;
}

void createlist(listtype list[], int& numlist)
{
	for (int i = 0; i < maxlist - 1; i++)
	{
		list[i] = initrec;
	}
	numlist = 0;
}

void insertlist(listtype list[], int& numlist, int id, string first, string last,
	string department, double salary)
{
	if (!fulllist(numlist))
	{
		//Search for where to insert
		int Index = 0;
		
		while ((Index < numlist) && (id > list[Index].id))
		{
			Index++;
		}
		
		//Move all down 1
		for (int i = numlist - 1; i >= Index; i--)
		{
			list[i + 1] = list[i];
		}

		//Move data into index position
		list[Index].id = id;
		list[Index].first = first;
		list[Index].last = last;
		list[Index].department = department;
		list[Index].salary = salary;
		numlist++;
	}
	else cout << "Cannot insert, list is full.\n";
}

void readem(listtype list[], int& numlist)
{
	ifstream infile("In.txt");

	int id;
	string first, last, department;
	double salary;
	while (!infile.eof())
	{
		infile >> id >> first >> last >> department >> salary;
		insertlist(list, numlist, id, first, last, department, salary);
	}
}

void traverselist(listtype list[], int& numlist, ofstream& outfile)
{
	if (!emptylist(numlist))
	{
		outfile << left << setw(5) << "ID" <<
			setw(15) << "Name" <<
			setw(13) << "Department" <<
			setw(7) << "Salary" << endl;
		for (int i = 0; i < numlist - 1; i++)
		{
			string firstlast = list[i].first + " " + list[i].last;
			outfile << setw(5) << list[i].id <<
				setw(15) << firstlast <<
				setw(13) << list[i].department <<
				setw(7) << list[i].salary << endl;
		}
	}
	else
	{
		outfile << "List is Empty\n";
	}
	outfile << endl;
}

void deletelist(listtype list[], int &numlist, int id)
{
	int Index = 0;

	//search for where to delete
	while ((Index < numlist) && (id > list[Index].id))
	{
		Index++;
	}

	//if found move all below up one
	if (id == list[Index].id)
	{
		for (int i = Index; i <= numlist; i++)
		{
			list[i] = list[i + 1];
		}
		numlist--;
	}
	else cout << "Cannot delete, there is no id with that number.\n";
}

int main()
{
	int numlist;
	listtype list[maxlist];
	string cities = "";

	ofstream outfile("Out.txt");

	createlist(list, numlist);
	readem(list, numlist);
	traverselist(list, numlist, outfile);
	insertlist(list, numlist, 7676, "Charles", "Darwin", "Biology", 50000);
	deletelist(list, numlist, 3966);
	traverselist(list, numlist, outfile);
	return 0;
}