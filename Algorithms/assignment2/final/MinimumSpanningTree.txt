Minimum Spanning Tree
Edge 1
City 1: San Francisco
City 2: Youngstown
Distance 47

Edge 2
City 1: Wisconsin Dells
City 2: San Francisco
Distance 55

Edge 3
City 1: San Bernardino
City 2: Wisconsin Dells
Distance 54

Edge 4
City 1: Rockford
City 2: San Bernardino
Distance 72

Edge 5
City 1: Saint Cloud
City 2: Wisconsin Dells
Distance 75

Edge 6
City 1: Vincennes
City 2: San Francisco
Distance 84

Edge 7
City 1: Sioux City
City 2: Youngstown
Distance 88

Edge 8
City 1: South Bend
City 2: Sioux City
Distance 67

Edge 9
City 1: Rutland
City 2: Wisconsin Dells
Distance 103

Edge 10
City 1: Waco
City 2: Rutland
Distance 90

Edge 11
City 1: San Jose
City 2: Waco
Distance 80

Edge 12
City 1: Worcester
City 2: San Jose
Distance 102

Edge 13
City 1: San Diego
City 2: Worcester
Distance 90

Edge 14
City 1: Ravenna
City 2: Rutland
Distance 105

Edge 15
City 1: Reading
City 2: Ravenna
Distance 34

Edge 16
City 1: Rochester
City 2: Reading
Distance 57

Edge 17
City 1: Springfield
City 2: Reading
Distance 72

Edge 18
City 1: Springfield
City 2: Ravenna
Distance 80

Edge 19
City 1: Scottsbluff
City 2: Reading
Distance 87

Edge 20
City 1: Stevens Point
City 2: Rochester
Distance 88

Edge 21
City 1: Twin Falls
City 2: Stevens Point
Distance 33

Edge 22
City 1: Steubenville
City 2: Stevens Point
Distance 60

Edge 23
City 1: Trenton
City 2: Steubenville
Distance 25

Edge 24
City 1: Valdosta
City 2: Trenton
Distance 60

Edge 25
City 1: Saint Joseph
City 2: Valdosta
Distance 44

Edge 26
City 1: Wheeling
City 2: Valdosta
Distance 63

Edge 27
City 1: Watertown
City 2: Steubenville
Distance 72

Edge 28
City 1: Seattle
City 2: Watertown
Distance 30

Edge 29
City 1: Syracuse
City 2: Stevens Point
Distance 78

Edge 30
City 1: Weed
City 2: Syracuse
Distance 51

Edge 31
City 1: Richmond
City 2: Syracuse
Distance 64

Edge 32
City 1: Victoria
City 2: Syracuse
Distance 71

Edge 33
City 1: Sarasota
City 2: Victoria
Distance 53

Edge 34
City 1: Washington
City 2: Weed
Distance 73

Edge 35
City 1: Selma
City 2: Valdosta
Distance 85

Edge 36
City 1: Wichita
City 2: Wheeling
Distance 85

Edge 37
City 1: Rochester
City 2: Wichita
Distance 80

Edge 38
City 1: Saint Joseph
City 2: Syracuse
Distance 85

Edge 39
City 1: Salina
City 2: Sarasota
Distance 87

Edge 40
City 1: Rhinelander
City 2: Steubenville
Distance 92

Edge 41
City 1: Salem
City 2: Rhinelander
Distance 59

Edge 42
City 1: Scranton
City 2: Springfield
Distance 93

Edge 43
City 1: Wausau
City 2: Scranton
Distance 42

Edge 44
City 1: Terre Haute
City 2: Wausau
Distance 58

Edge 45
City 1: Staunton
City 2: Syracuse
Distance 93

Edge 46
City 1: Saint Louis
City 2: Valdosta
Distance 102

Edge 47
City 1: Schenectady
City 2: Wichita
Distance 106

Edge 48
City 1: Trinidad
City 2: Schenectady
Distance 78

Edge 49
City 1: Traverse City
City 2: Reading
Distance 109

Edge 50
City 1: West Palm Beach
City 2: Washington
Distance 109

Edge 51
City 1: Richmond
City 2: San Diego
Distance 110

Edge 52
City 1: Vancouver
City 2: Scranton
Distance 111

Edge 53
City 1: Shreveport
City 2: Vancouver
Distance 99

Edge 54
City 1: Waukegan
City 2: Shreveport
Distance 73

Edge 55
City 1: Uniontown
City 2: Waukegan
Distance 69

Edge 56
City 1: Waterbury
City 2: Waukegan
Distance 98

Edge 57
City 1: Savannah
City 2: Waterbury
Distance 94

Edge 58
City 1: Sterling
City 2: Savannah
Distance 109

Edge 59
City 1: Springfield
City 2: Sterling
Distance 50

Edge 60
City 1: Tuscaloosa
City 2: Springfield
Distance 56

Edge 61
City 1: San Antonio
City 2: Sterling
Distance 113

Edge 62
City 1: Rock Springs
City 2: Reading
Distance 115

Edge 63
City 1: Utica
City 2: Uniontown
Distance 116

Edge 64
City 1: Williamson
City 2: Utica
Distance 83

Edge 65
City 1: Stroudsburg
City 2: Williamson
Distance 69

Edge 66
City 1: Swainsboro
City 2: Utica
Distance 105

Edge 67
City 1: Topeka
City 2: Stroudsburg
Distance 105

Edge 68
City 1: Wenatchee
City 2: Wichita
Distance 117

Edge 69
City 1: Tallahassee
City 2: Wenatchee
Distance 82

Edge 70
City 1: Warren
City 2: Washington
Distance 118

Edge 71
City 1: Winston-Salem
City 2: Richmond
Distance 120

Edge 72
City 1: Sacramento
City 2: Rockford
Distance 122

Edge 73
City 1: Texarkana
City 2: Sacramento
Distance 47

Edge 74
City 1: Waycross
City 2: Sacramento
Distance 94

Edge 75
City 1: Waterloo
City 2: Sacramento
Distance 103

Edge 76
City 1: Sandusky
City 2: San Francisco
Distance 125

Edge 77
City 1: Tupelo
City 2: Sandusky
Distance 54

Edge 78
City 1: Toronto
City 2: South Bend
Distance 126

Edge 79
City 1: Sioux Falls
City 2: Toronto
Distance 117

Edge 80
City 1: Spokane
City 2: Sioux Falls
Distance 82

Edge 81
City 1: Santa Rosa
City 2: Waterbury
Distance 126

Edge 82
City 1: Springfield
City 2: Wichita
Distance 127

Edge 83
City 1: Rocky Mount
City 2: Saint Joseph
Distance 127

Edge 84
City 1: Santa Ana
City 2: Youngstown
Distance 128

Edge 85
City 1: Saint Augustine
City 2: Santa Ana
Distance 115

Edge 86
City 1: Sault Sainte Marie
City 2: Saint Joseph
Distance 129

Edge 87
City 1: Red Bluff
City 2: Wichita
Distance 131

Edge 88
City 1: Saint Johnsbury
City 2: Red Bluff
Distance 105

Edge 89
City 1: Walla Walla
City 2: Waterbury
Distance 132

Edge 90
City 1: Williamsport
City 2: Texarkana
Distance 136

Edge 91
City 1: Sedalia
City 2: Williamsport
Distance 115

Edge 92
City 1: Tacoma
City 2: Terre Haute
Distance 138

Edge 93
City 1: Roanoke
City 2: Tacoma
Distance 88

Edge 94
City 1: Roswell
City 2: Roanoke
Distance 109

Edge 95
City 1: Reno
City 2: Williamson
Distance 139

Edge 96
City 1: Seminole
City 2: Seattle
Distance 148

Edge 97
City 1: Vicksburg
City 2: Seminole
Distance 95

Edge 98
City 1: Yankton
City 2: Seminole
Distance 127

Edge 99
City 1: Saginaw
City 2: Sterling
Distance 150

Edge 100
City 1: Stockton
City 2: Saginaw
Distance 140

Edge 101
City 1: Sherman
City 2: Stockton
Distance 117

Edge 102
City 1: Valley City
City 2: Sherman
Distance 135

Edge 103
City 1: Wilmington
City 2: Rochester
Distance 152

Edge 104
City 1: Sumter
City 2: Schenectady
Distance 152

Edge 105
City 1: Tulsa
City 2: Scranton
Distance 154

Edge 106
City 1: Sheridan
City 2: Rutland
Distance 156

Edge 107
City 1: Watertown
City 2: Wausau
Distance 156

Edge 108
City 1: Salida
City 2: Syracuse
Distance 157

Edge 109
City 1: Salinas
City 2: Williamson
Distance 160

Edge 110
City 1: Wilmington
City 2: Salinas
Distance 58

Edge 111
City 1: Winchester
City 2: Salinas
Distance 105

Edge 112
City 1: Tyler
City 2: Wilmington
Distance 134

Edge 113
City 1: Tucson
City 2: Syracuse
Distance 163

Edge 114
City 1: Salisbury
City 2: Terre Haute
Distance 163

Edge 115
City 1: Santa Barbara
City 2: Salisbury
Distance 103

Edge 116
City 1: Richfield
City 2: Waterloo
Distance 164

Edge 117
City 1: Toledo
City 2: Topeka
Distance 170

Edge 118
City 1: Yakima
City 2: Stroudsburg
Distance 182

Edge 119
City 1: Saint Paul
City 2: Schenectady
Distance 183

Edge 120
City 1: Regina
City 2: Rocky Mount
Distance 188

Edge 121
City 1: Santa Fe
City 2: Tupelo
Distance 193

Edge 122
City 1: Salt Lake City
City 2: Rochester
Distance 201

Edge 123
City 1: Williston
City 2: Utica
Distance 201

Edge 124
City 1: Tampa
City 2: Valdosta
Distance 217

Edge 125
City 1: San Angelo
City 2: Youngstown
Distance 218

Edge 126
City 1: Wichita Falls
City 2: Salida
Distance 227

Edge 127
City 1: Winnipeg
City 2: Williamson
Distance 240

Total Weight 13506
