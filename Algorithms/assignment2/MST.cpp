#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sstream>

using namespace std;

const int maxv = 128;

enum colortype { white, grey, black };

void initcolor(colortype color[]) {
	for (int i = 0; i < maxv; i++) {
		color[i] = white;
	}
}

//queue stuff
///////////////////////////////////////////////////////
struct edgetype {
	string v1, v2;
	int wt;
};

struct qtype {
	string v1, v2;
	int wt;
	qtype* link;
};

void createq(qtype*& qhead, qtype*& qtail) {
	qhead = new qtype;
	qtail = new qtype;
	qhead->link = qtail;
	qtail->link = NULL;
}

bool emptyq(qtype* qhead, qtype* qtail) {
	return qhead->link == qtail;
}

edgetype deq(qtype* qhead, qtype* qtail) {
	edgetype u;
	qtype* c;
	c = qhead->link;
	qhead->link = c->link;
	u.v1 = c->v1;
	u.v2 = c->v2;
	u.wt = c->wt;
	delete c;
	return u;
}

void enq(qtype* qhead, qtype* qtail, string v1, string v2, int wt) {
	qtype* knew, * next, * previous;
	knew = new qtype;
	knew->v1 = v1;
	knew->v2 = v2;
	knew->wt = wt;
	previous = qhead;
	next = qhead->link;
	while (next != qtail && wt >= next->wt) {
		previous = next;
		next = next->link;
	}
	previous->link = knew;
	knew->link = next;
}
//////////////////////////////////////////////////////////////////////////////////////////

void creategraph(int adj[][maxv]) {
	for (int i = 0; i < maxv; i++) {
		for (int j = 0; j < maxv; j++) {
			adj[i][j] = 0;
		}
	}
}

void formatted_input(int adj[][maxv]) {
	ifstream inf("distances.txt");
	ofstream outf("formattedinput.txt");
	int numcount;
	int city = -1;
	int graph [200][200];
	string current_city_name, previous_city_name;
	while(!inf.eof()){
		string line = "";
		getline(inf, line);
		if(line[0] == '*') continue;
		if(isdigit(line[0]) == false){
			city++;
			numcount = 0;
			// cout << "current city: " << city << endl;
			previous_city_name = current_city_name;
			current_city_name = line.substr(0,line.find(','));
			cout << "current_city_name: " << current_city_name << endl;

		}
		if(isdigit(line[0]) == true){
			// cout << "number\n";
			stringstream ss(line);
    		string num;
			
    		while (ss >> num) {
        	// cout << num << endl;
			graph[city][numcount] = stoi(num);
			numcount++;
    		}
		}
		
		outf << line.substr(0,line.find(',')) << endl;
	}
	// cout << "total cities: " << city + 1 << endl;
	// cout << "numcount: " << numcount << endl;
	// cout << "test\n";
	for(int i = 0; i < city + 1; i++){
		for(int j = 0; j < city; j++){
			// cout << graph[i][j] << " ";
			int v1 = i;
			int v2 = j;
			int wt = graph[i][j];
		
			adj[v1][v2] = wt;
			adj[v2][v1] = wt;
		}
		// cout << "end of city: "<< i << endl;
	}
	// cout << endl;
	// cout << "total cities: " << city + 1 << endl;
	outf.close();
}

// made some adjustments but needs a few fixes, compare with original
// will need to change numcount to something else 
// void algo(qtype *qhead, qtype *qtail, edgetype ans[], colortype color[], int adj[][maxv], string start, int numcount) {
// 	int counter = 0;
// 	enq(qhead, qtail, start, "test", 0);
// 	while (!emptyq(qhead, qtail)) {
// 		edgetype e = deq(qhead,qtail);
// 		if (e.wt != 0 && color[numcount] != black) {
// 			ans[counter] = e;
// 			counter++;
// 		}
// 		if (color[numcount]] != black) {
// 			for (int i = 0; i <= numcount; i++) {
// 				if (adj[numcount][i] != 0 && color[i] != black) {
// 					enq(qhead, qtail, i, e.v1, adj[e.v1][i]);
// 					color[i] = grey;
// 				}
// 			}
// 			color[e.v1] = black;
// 		}
// 	}
// }

int nthSubstr(int n, const string& s,
              const string& p) {
   string::size_type i = s.find(p);     // Find the first occurrence

   int j;
   for (j = 1; j < n && i != string::npos; ++j)
      i = s.find(p, i+1); // Find the next occurrence

   if (j == n)
     return(i);
   else
     return(-1);
}

const int valuequeue(int adj[][maxv], qtype* qhead, qtype* qtail) {
	ifstream inf("distances.txt");
	ofstream outf("valuequeue.txt");
	// numcount counts total cities found
	int numcount;
	string cities = "";
	int city = -1;
	string current_city_name, previous_city_name;
	while(!inf.eof()){
		string line = "";
		getline(inf, line);
		if(line[0] == '*') continue;
		if(isdigit(line[0]) == false){
			city++;
			numcount = 0;
			if (city == 0 || city == 1){
				cities = current_city_name;
			}else{
				cities += "," + current_city_name;
			}
			current_city_name = line.substr(0,line.find(','));
		}
		if(isdigit(line[0]) == true){
			stringstream ss(line);
    		string num;
			// counter tracks the number of distances found for each city
			unsigned int counter = 0;
			istringstream ss1(cities);
			string city_name;

			while(getline(ss1, city_name, ',')) {
				counter++;
			}
    		while (ss >> num) {
			string second_city = cities.substr(nthSubstr(numcount,cities,",") + 1,nthSubstr(numcount + 1,cities,",") - nthSubstr(numcount,cities,",") - 1);

			enq(qhead, qtail, current_city_name, second_city, stoi(num));
    		
			// testing to check if correct values are enqued
			edgetype e = deq(qhead, qtail);
    		outf << e.v1 << " " << e.v2 << " " << e.wt << endl;
			
			numcount++;
    		}
		}
	}
	return numcount;
}

void adj_matrix() {
	int adj[200][maxv];
	ifstream inf("distances.txt");
	ofstream outf("adjmatrix.txt");
	int numcount;
	int city = -1;
	int graph [200][200];
	while(!inf.eof()){
		string line = "";
		getline(inf, line);
		if(line[0] == '*') continue;
		if(isdigit(line[0]) == false){
			city++;
			numcount = 0;
		}
		if(isdigit(line[0]) == true){
			stringstream ss(line);
    		string num;
			
    		while (ss >> num) {
			graph[city][numcount] = stoi(num);
			numcount++;
    		}
		}
	}

	for(int i = 0; i < city + 1; i++){
		for(int j = 0; j < city ; j++){
			int v1 = i;
			int v2 = j;
			int wt = graph[i][j];
		
			adj[v1][v2] = wt;
			adj[v2][v1] = wt;
		}
	}

	//top labels
	for (int j = 0; j < maxv; j++) {
		outf << setw(5) << left << j;
	}
	outf << endl;
	
	for (int i = 0; i < maxv; i++) {
		//side labels
        outf  << setw(5) << i;
		//contents
		for (int j = 0; j < maxv; j++) {
			outf << setw(5) << adj[i][j];
		}
		outf << endl;
	}
	outf << endl;
	outf.close();
}

void adj_list() {
	int adj[200][maxv];
	ifstream inf("distances.txt");
	ofstream outf("adjacencylist.txt");
	int numcount;
	int city = -1;
	int graph [200][200];
	while(!inf.eof()){
		string line = "";
		getline(inf, line);
		if(line[0] == '*') continue;
		if(isdigit(line[0]) == false){
			city++;
			numcount = 0;
		}
		if(isdigit(line[0]) == true){
			stringstream ss(line);
    		string num;
			
    		while (ss >> num) {
			graph[city][numcount] = stoi(num);
			numcount++;
    		}
		}
	}
	for(int i = 0; i < city + 1; i++){
		for(int j = 0; j < city ; j++){
			int v1 = i;
			int v2 = j;
			int wt = graph[i][j];
		
			adj[v1][v2] = wt;
			adj[v2][v1] = wt;
		}
	}
	for(int i = 0; i < city + 1; i++){
		for(int j = 0; j < city; j++){
			outf << i << " " << j << " " << adj[i][j] << endl;
		}
	}
	outf.close();
}

int main(){
	qtype *qhead, *qtail;
	createq(qhead, qtail);

    int adj[maxv][maxv];

	valuequeue(adj, qhead, qtail);
    // adj_matrix();
	// adj_list();

	// testing
	// const int x = valuequeue(adj, qhead, qtail);
	// colortype color[x];
	// initcolor(color);
    return 0;
}