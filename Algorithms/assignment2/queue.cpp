#include <iostream>

using namespace std;

struct edgetype {
	string v1, v2;
	int wt;
};

struct qtype {
	string v1, v2;
	int wt;
	qtype* link;
};

void createq(qtype*& qhead, qtype*& qtail) {
	qhead = new qtype;
	qtail = new qtype;
	qhead->link = qtail;
	qtail->link = NULL;
}

bool emptyq(qtype* qhead, qtype* qtail) {
	return qhead->link == qtail;
}

edgetype deq(qtype* qhead, qtype* qtail) {
	edgetype u;
	qtype* c;
	c = qhead->link;
	qhead->link = c->link;
	u.v1 = c->v1;
	u.v2 = c->v2;
	u.wt = c->wt;
	delete c;
	return u;
}

void enq(qtype* qhead, qtype* qtail, string v1, string v2, int wt) {
	qtype* knew, * next, * previous;
	knew = new qtype;
	knew->v1 = v1;
	knew->v2 = v2;
	knew->wt = wt;
	previous = qhead;
	next = qhead->link;
	while (next != qtail && wt >= next->wt) {
		previous = next;
		next = next->link;
	}
	previous->link = knew;
	knew->link = next;
}

int main(){
    qtype *qhead, *qtail;
	createq(qhead, qtail);
    enq(qhead, qtail, "Concord", "Lexington", 0);
    enq(qhead, qtail, "New York", "Denver", 0);
    enq(qhead, qtail, "Tucson", "Miami", 0);

	// store values of city name in a linked list then set the second city as the appropriate value accordingly

    edgetype e = deq(qhead, qtail);
    cout << e.v1 << " " << e.v2 << " " << e.wt << endl;
	
	e = deq(qhead, qtail);
    cout << e.v1 << " " << e.v2 << " " << e.wt << endl;

	e = deq(qhead, qtail);
    cout << e.v1 << " " << e.v2 << " " << e.wt << endl;
    return 0;
}