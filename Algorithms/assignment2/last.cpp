#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sstream>

using namespace std;

enum colortype { white, grey, black };
// const int vertices = 10000;

//queue stuff
///////////////////////////////////////////////////////
struct edgetype {
	string v1, v2;
	int v1_number, v2_number;
	int wt;
};

struct qtype {
	string v1, v2;
	int v1_number, v2_number;
	int wt;
	qtype* link;
};

void createq(qtype*& qhead, qtype*& qtail) {
	qhead = new qtype;
	qtail = new qtype;
	qhead->link = qtail;
	qtail->link = NULL;
}

bool emptyq(qtype* qhead, qtype* qtail) {
	return qhead->link == qtail;
}

edgetype deq(qtype* qhead, qtype* qtail) {
	edgetype u;
	qtype* c;
	c = qhead->link;
	qhead->link = c->link;
	u.v1 = c->v1;
	u.v2 = c->v2;
	u.v1_number = c->v1_number;
	u.v2_number = c->v2_number;
	u.wt = c->wt;
	delete c;
	return u;
}

void enq(qtype* qhead, qtype* qtail, string v1, string v2, int v1_number, int v2_number, int wt) {
	qtype* knew, * next, * previous;
	knew = new qtype;
	knew->v1 = v1;
	knew->v2 = v2;
	knew->v1_number = v1_number;
	knew->v2_number = v2_number;
	knew->wt = wt;
	previous = qhead;
	next = qhead->link;
	while (next != qtail && wt >= next->wt) {
		previous = next;
		next = next->link;
	}
	previous->link = knew;
	knew->link = next;
}
//////////////////////////////////////////////////////////////////////////////////////////

void initcolor(colortype color[], int x) {
	for (int i = 0; i < x; i++) {
		color[i] = white;
	}
}

void initAns(edgetype ans[], int vertices) {
	for (int i = 0; i < vertices; i++) {
		ans[i].v1 = '0';
		ans[i].v2 = '0';
		ans[i].wt = 0;
	}
}

int nthSubstr(int n, const string& s,
              const string& p) {
   string::size_type i = s.find(p);     // Find the first occurrence

   int j;
   for (j = 1; j < n && i != string::npos; ++j)
      i = s.find(p, i+1); // Find the next occurrence

   if (j == n)
     return(i);
   else
     return(-1);
}

const int valuequeue(qtype* qhead, qtype* qtail) {
	ifstream inf("distances.txt");
	ofstream outf("valuequeue.txt");
	// numcount counts total cities found
	int numcount;
	string cities = "";
	int city = -1;
	string current_city_name, previous_city_name;
	while(!inf.eof()){
		string line = "";
		getline(inf, line);
		if(line[0] == '*') continue;
		if(isdigit(line[0]) == false){
			city++;
			if(city == 1){
				enq(qhead, qtail, current_city_name, current_city_name, 0, 0, 0);
			}
			numcount = 0;
			if (city == 0 || city == 1){
				cities = current_city_name;
			}else{
				cities += "," + current_city_name;
			}
			current_city_name = line.substr(0,line.find(','));
		}
		if(isdigit(line[0]) == true){
			stringstream ss(line);
    		string num;
			// counter tracks the number of distances found for each city
			unsigned int counter = 0;
			istringstream ss1(cities);
			string city_name;

			while(getline(ss1, city_name, ',')) {
				counter++;
			}
    		while (ss >> num) {
			string second_city = cities.substr(nthSubstr(numcount,cities,",") + 1,nthSubstr(numcount + 1,cities,",") - nthSubstr(numcount,cities,",") - 1);

			enq(qhead, qtail, current_city_name, second_city, city, numcount, stoi(num));
    		
			// testing to check if correct values are enqued
			// edgetype e = deq(qhead, qtail);
    		// outf << e.v1 << " " << e.v2 << " " << e.wt << endl;
			
			numcount++;
    		}
		}
	}
	return numcount;
}

void print(int **arr, int n, int m)
{
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<m;j++)
            cout<<arr[i][j]<<" ";
        cout<<endl;
    }
}

void new_matrix(int **arr, int n, int m,qtype* qhead, qtype* qtail)
{
	string cities [n];
	// cities[0] = "YOUNGSTOWN";
	for(int i = 0; i < n; i++){
		cities[i] = "";
	}

	ofstream outf("newadjmatrix.txt");
	// need to figure out how to print out names of cities and the values of the distances in a matrix
	// maybe add number value to nodes then set that number value in the array to the value in the matrix 
    
	while(!emptyq(qhead, qtail)){
		edgetype e = deq(qhead, qtail);
		// cout << e.v1_number<< " " << e.v1 << " " << e.v2_number << " " << e.v2 << " " << e.wt << endl;
		arr[e.v1_number][e.v2_number] = e.wt;
		arr[e.v2_number][e.v1_number] = e.wt;
		
		cities[e.v1_number] = e.v1;
	}
	
	for(int i = 0; i < n; i++){
		cout << cities[i] << endl;
	}

	int longest_name = 0;
	for(int i = 0; i < m; i++){
		if(cities[i].length() > longest_name){
			longest_name = cities[i].length();
		}
	}

	//top labels
	outf << setw(longest_name + 1) << "";
	for (int j = 0; j < m; j++) {
		outf << setw(cities[j].length() + 1) << left << cities[j];
	}
	outf << endl;
	
	for (int i = 0; i < n; i++) {
		//side labels
        outf  << setw(longest_name + 1) << cities[i];
		//contents
		for (int j = 0; j < m; j++) {
			outf << setw(cities[j].length() + 1) << arr[i][j];
		}
		outf << endl;
	}
	outf << endl;
	outf.close();
}

void algo(qtype *qhead, qtype *qtail, edgetype ans[], colortype color[], int **arr, string start, int numcount) {
	int counter = 0;
	// enq(qhead, qtail, start, second_city, city, numcount, stoi(num));
	enq(qhead, qtail, start, "0", 0, 0, 0);
	// enq(qhead, qtail, start, '0', 0);
	while (!emptyq(qhead, qtail)) {
		edgetype e = deq(qhead,qtail);
		//need to insert correct city
		// cout << e.v1_number << endl;
		if (e.wt != 0 && color[e.v1_number] != black) {
			ans[counter] = e;
			counter++;
		}
		if (color[e.v1_number] != black) {
			for (int i = 0; i <= numcount; i++) {
				if (arr[e.v1_number][i] != 0 && color[i] != black) {
					// enq(qhead, qtail, i, e.v1_number, arr[e.v1_number][i]);
					// enq(qhead, qtail, current_city_name, second_city, city, numcount, stoi(num));
					enq(qhead, qtail, e.v1, e.v2, e.v1_number, e.v2_number, e.wt);
					color[i] = grey;
				}
			}
			color[e.v1_number] = black;
		}
	}
}

void printans(edgetype ans[],  int x) {
	int totalwt = 0;
	ofstream outf("MinimumSpanningTree.txt");
	outf << "Minimum Spanning Tree\n";
	for (int i = 0; i < x; i++) {
		if (ans[i].wt != 0) {
			outf << "Edge " << i + 1 << endl;
			outf << "v1 " << ans[i].v1_number << endl;
			outf << "v2 " << ans[i].v2_number << endl;
			outf << "wt " << ans[i].wt << endl;
			outf << endl;
			totalwt += ans[i].wt;
		}
	}
	outf << "Total Weight " << totalwt;
	outf << endl;
}

string* getNames() {
 string* names = new string[3];
 names[0] = "Simon";
 names[1] = "Peter";
 names[2] = "Dave"; 

 return names;
}

int main(){
    string * cities = getnames();
    cout << cities[0] << endl;


	qtype *qhead, *qtail;
	createq(qhead, qtail);

    // int adj[maxv][maxv];

	// valuequeue(adj, qhead, qtail);
    // adj_matrix();
	// adj_list();

	// testing
	const int x = valuequeue(qhead, qtail);
	// colortype color[x];
	// initcolor(color);

    // adj_matrix(test);
	int n = x + 1;
	int m = x + 1;

    int **arr = new int*[n];
	
    //zero out array
    for(int i = 0; i < n; i++)
    {
        arr[i]=new int[m];
        for(int j = 0; j < m; j++)
            arr[i][j] = 0;
    }
	
    new_matrix(arr, n, m, qhead, qtail);  //This statement will pass the array as parameter.

	// queue testing
	// while(!emptyq(qhead,qtail)){
	// 	edgetype e = deq(qhead,qtail);
	// 	cout << e.v1 << " " << e.v1_number << " " << e.v2 << " " << e.v2_number << " " << e.wt << endl;
	// }

	//currently working on
	edgetype ans[x];
	initAns(ans, x);

	colortype color[x];
	initcolor(color, x);

	algo(qhead, qtail, ans, color, arr, "Youngstown", x);
	printans(ans, x);
	
    //dynamic array cleanup after use
    for(int i=0;i<n;i++)
        delete [] arr[i];
    delete [] arr;

    return 0;
}