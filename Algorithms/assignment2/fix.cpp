#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

void creategraph(int **adj, int maxv) {
	for (int i = 0; i < maxv; i++) {
		for (int j = 0; j < maxv; j++) {
			adj[i][j] = 0;
		}
	}
}

void readem(int ** adj, int maxv) {
	ifstream inf("graphin.txt");
	char v1 = '0';
	char v2 = '0';
	int wt = 0;
	while (!inf.eof()) {
		inf >> v1 >> v2 >> wt;
		adj[v1][v2] = wt;
		adj[v2][v1] = wt;
	}
}

enum colortype { white, grey, black };

void initcolor(colortype color[], int maxv) {
	for (int i = 0; i < maxv; i++) {
		color[i] = white;
	}
}

struct edgetype {
	string v1, v2;
	int v1_number, v2_number;
	int wt;
};

struct qtype {
	string v1, v2;
	int v1_number, v2_number;
	int wt;
	qtype* link;
};

void printgraph(int ** adj, ofstream& outf, int maxv) {
	outf << " ";
	for (int j = 'A'; j < maxv; j++) {
		outf << setw(2) << static_cast<char>(j);
	}
	outf << endl;
	
	for (int i = 'A'; i < maxv; i++) {
		outf  << static_cast<char>(i) << " ";
		for (int j = 'A'; j < maxv; j++) {
			outf << adj[i][j] << " ";
		}
		outf << endl;
	}
	outf << endl;
}

void initAns(edgetype ans[], int vertices) {
	for (int i = 0; i < vertices; i++) {
		ans[i].v1 = "0";
		ans[i].v2 = "0";
        ans[i].v1_number = 0;
        ans[i].v2_number = 0;
		ans[i].wt = 0;
	}
}

void createq(qtype*& qhead, qtype*& qtail) {
	qhead = new qtype;
	qtail = new qtype;
	qhead->link = qtail;
	qtail->link = NULL;
}

bool emptyq(qtype* qhead, qtype* qtail) {
	return qhead->link == qtail;
}

edgetype deq(qtype* qhead, qtype* qtail) {
	edgetype u;
	qtype* c;
	c = qhead->link;
	qhead->link = c->link;
	u.v1 = c->v1;
	u.v2 = c->v2;
	u.v1_number = c->v1_number;
	u.v2_number = c->v2_number;
	u.wt = c->wt;
	delete c;
	return u;
}

void enq(qtype* qhead, qtype* qtail, string v1, string v2, int v1_number, int v2_number, int wt) {
	qtype* knew, * next, * previous;
	knew = new qtype;
	knew->v1 = v1;
	knew->v2 = v2;
	knew->v1_number = v1_number;
	knew->v2_number = v2_number;
	knew->wt = wt;
	previous = qhead;
	next = qhead->link;
	while (next != qtail && wt >= next->wt) {
		previous = next;
		next = next->link;
	}
	previous->link = knew;
	knew->link = next;
}

int getcity(string * city, string find, int x){
	int num = 0;
	for(int i = 0; i < x + 1; i++){
		if (find == city[i]){
			return i;
		}
	}
	
	return num;
}

int nthSubstr(int n, const string& s,
              const string& p) {
   string::size_type i = s.find(p);     // Find the first occurrence

   int j;
   for (j = 1; j < n && i != string::npos; ++j)
      i = s.find(p, i+1); // Find the next occurrence

   if (j == n)
     return(i);
   else
     return(-1);
}

const int valuequeue(qtype* qhead, qtype* qtail) {
	ifstream inf("distances.txt");
	ofstream outf("valuequeue.txt");
	// numcount counts total cities found
	int numcount;
	string cities = "";
	int city = -1;
	string current_city_name, previous_city_name;
	while(!inf.eof()){
		string line = "";
		getline(inf, line);
		if(line[0] == '*') continue;
		if(isdigit(line[0]) == false){
			city++;
			if(city == 1){
				enq(qhead, qtail, current_city_name, current_city_name, 0, 0, 0);
			}
			numcount = 0;
			if (city == 0 || city == 1){
				cities = current_city_name;
			}else{
				cities += "," + current_city_name;
			}
			current_city_name = line.substr(0,line.find(','));
		}
		if(isdigit(line[0]) == true){
			stringstream ss(line);
    		string num;
			// counter tracks the number of distances found for each city
			unsigned int counter = 0;
			istringstream ss1(cities);
			string city_name;

			while(getline(ss1, city_name, ',')) {
				counter++;
			}
    		while (ss >> num) {
			string second_city = cities.substr(nthSubstr(numcount,cities,",") + 1,nthSubstr(numcount + 1,cities,",") - nthSubstr(numcount,cities,",") - 1);

			enq(qhead, qtail, current_city_name, second_city, city, numcount, stoi(num));
    		
			// testing to check if correct values are enqued
			// edgetype e = deq(qhead, qtail);
    		// outf << e.v1 << " " << e.v2 << " " << e.wt << endl;
			
			numcount++;
    		}
		}
	}
	return numcount;
}

string* new_matrix(int **arr, int n, int m,qtype* qhead, qtype* qtail)
{
	string * cities = new string[n];
	// cities[0] = "YOUNGSTOWN";	
	// for(int i = 0; i < n; i++){
	// 	cities[i] = "";
	// }

	ofstream outf("newadjmatrix.txt");
	// need to figure out how to print out names of cities and the values of the distances in a matrix
	// maybe add number value to nodes then set that number value in the array to the value in the matrix 
    
	while(!emptyq(qhead, qtail)){
		edgetype e = deq(qhead, qtail);
		// cout << e.v1_number<< " " << e.v1 << " " << e.v2_number << " " << e.v2 << " " << e.wt << endl;
		arr[e.v1_number][e.v2_number] = e.wt;
		arr[e.v2_number][e.v1_number] = e.wt;
		
		cities[e.v1_number] = e.v1;
	}
	
	// for(int i = 0; i < n; i++){
	// 	cout << cities[i] << endl;
	// }

	int longest_name = 0;
	for(int i = 0; i < m; i++){
		if(cities[i].length() > longest_name){
			longest_name = cities[i].length();
		}
	}

	//top labels
	outf << setw(longest_name + 1) << "";
	for (int j = 0; j < m; j++) {
		outf << setw(cities[j].length() + 1) << left << cities[j];
	}
	outf << endl;
	
	for (int i = 0; i < n; i++) {
		//side labels
        outf  << setw(longest_name + 1) << cities[i];
		//contents
		for (int j = 0; j < m; j++) {
			outf << setw(cities[j].length() + 1) << arr[i][j];
		}
		outf << endl;
	}
	outf << endl;
	outf.close();
	return cities;
}

void algo(qtype *qhead, qtype *qtail, edgetype ans[], colortype color[], int** adj, string start, int x, string *cities) {
	int counter = 0;
	cout << "TEST2\n";
	enq(qhead, qtail, start, "0", 0, 0, 0);
	while (!emptyq(qhead, qtail)) {
		edgetype e = deq(qhead,qtail);
        cout << e.v1 << " " << e.v1_number << " " << e.v2 << " " << e.v2_number << " " << e.wt << endl;
		if (e.wt != 0 && color[e.v1_number] != black) {
			ans[counter] = e;
			counter++;
			cout << "counter: " << counter << endl;

		}
        cout << "test\n";
		if (color[e.v1_number] != black) {
            cout << "222\n";
			for (int i = 0; i <= x; i++) {
                cout << "adj" << *adj[e.v1_number] << endl;
				if (adj[e.v1_number][i] != 0 && color[i] != black) {
                    cout << cities[i] << endl;
					enq(qhead, qtail,cities[i] , cities[e.v1_number], i, e.v1_number, adj[e.v1_number][i]);
					color[i] = grey;
				}
			}
			color[e.v1_number] = black;
		}
	}
}

void printans(edgetype ans[], ofstream &outf, int vertices) {
	int totalwt = 0;
	outf << "Minimum Spanning Tree\n";
	for (int i = 0; i < vertices; i++) {
		if (ans[i].wt != 0) {
			outf << "Edge " << i + 1 << endl;
			outf << "v1 " << ans[i].v1 << endl;
			outf << "v2 " << ans[i].v2 << endl;
			outf << "wt " << ans[i].wt << endl;
			outf << endl;
			totalwt += ans[i].wt;
		}
	}
	outf << "Total Weight " << totalwt;
	outf << endl;
}

int main() {
	ifstream inf("graphin.txt");
	ofstream outf("graphout.txt");

	
	// for (int i = 0; i < maxv; i++){
	// 	cout << color[i] << " " << i << endl;
	// }
    qtype *qhead, *qtail;
	createq(qhead, qtail);
	const int x = valuequeue(qhead, qtail);

    int n = x + 1;
	int m = x + 1;

    int **arr = new int*[n];
	
    //zero out array
    for(int i = 0; i < n; i++)
    {
        arr[i]=new int[m];
        for(int j = 0; j < m; j++)
            arr[i][j] = 0;
    }

	creategraph(arr);
	readem(arr);
	printgraph(arr, outf);

	edgetype ans[x];
	initAns(arr);

	

	colortype color[x];
	initcolor(color);
    
    
	
    string* cities = new_matrix(arr, n, m, qhead, qtail);  //This statement will pass the array as parameter.
	cout << "TEST\n";
    
	algo(qhead, qtail, ans, color, arr, "Youngstown", x, cities);
	printans(ans, outf);

	// for (int i = 0; i < maxv; i++){
	// 	cout << color[i] << " " << i << endl;
	// }

	return 0;
	system("pause");
}