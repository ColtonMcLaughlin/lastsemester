package sendmoremoney;

class digitize{
    public static int[] main(int word){
        int length = (String.valueOf(word)).length();

        int [] arr = {0,0,0,0,0,0,0,0,0,0};
        int rem = word;
        int number;
        int count = 0;

        int divisor;

        for (int i = length - 1; i >= 0; i--){
            divisor = (int)Math.pow(10, i);
            number = rem / divisor;
            rem = rem % divisor;
            arr[count] = number;
            count++;
        }

        return arr;
    }
}

class sendmoremoney {
    public static void main(String[] args) {
        int SEND;
        int S1,E1,N1,D1;
    
        int MORE;
        int M1,O1,R1,E2;

        int MONEY;
        int M2,O2,N2,E3,Y1;

        long begintime = System.nanoTime();
        for (SEND = 1000; SEND <= 9999; SEND++) {
       
            int [] result1 = digitize.main(SEND);
    
            S1 = result1[0];
            E1 = result1[1];
            N1 = result1[2];
            D1 = result1[3];
    
            //if e=a=t break out
            if (S1 == E1 || E1 == N1 || N1 == D1 || S1 == N1 || S1 == D1 || E1 == D1) continue;
            

            for (MORE = 1000; MORE <= 9999; MORE++) {
    
                int [] result2 = digitize.main(MORE);
                M1 = result2[0];
                O1 = result2[1];
                R1 = result2[2];
                E2 = result2[3];

                //if m=o=r=e break out
                if (M1 == O1 || O1 == R1 || R1 == E2 || M1 == O1 || M1 == E2 || M1 == E2 || N1 == R1 || S1 == R1 || E1 == R1 || D1 == R1) continue;

                MONEY = SEND + MORE;
    
                int [] result3 = digitize.main(MONEY);
    
                M2 = result3[0];
                O2 = result3[1];
                N2 = result3[2];
                E3 = result3[3];
                Y1 = result3[4];
                
                if ((String.valueOf(MONEY)).length() != 5) continue;
                
                if (M1 != M2 || O1 != O2 || E1 != E2 || E2 != E3 || N1 != N2) continue;
            
                if(M2 == O2 || O2 == N2 || N2 == E3 || E2 == Y1 || M2 == N2 || M2 == E3 || M2 == Y1 || O2 == E3 || O2 == Y1 || N2 == Y1 || S1 == R1) continue;

                System.out.println("SEND MORE MONEY " + "SEND: " + SEND + " MORE: " + MORE + " MONEY: " + MONEY);          
            }
        }
        long endtime = System.nanoTime();
        double timediff = endtime - begintime;
        System.out.println((double)timediff / 1000000000 + " seconds");
    }
} 