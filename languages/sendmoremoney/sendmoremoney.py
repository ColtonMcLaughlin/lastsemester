# to run enter python3 python_ETA.py in terminal
import time

def digitize(word):
    length = len(str(word))
    arr = [0,0,0,0,0,0,0,0,0,0]
    rem = word
    count = 0
    for i in range(length - 1, -1, -1):
        divisor = pow(10, i)
        number = (rem) / (divisor)
        rem = rem % divisor

        arr[count] = int(number)
        count+=1
    return arr

# print(time.time())
# begintime, endtime, timediff
begintime = time.time()
for SEND in range(1000, 10000):
    result1 = digitize(SEND)
    S1 = result1[0]
    E1 = result1[1]
    N1 = result1[2]
    D1 = result1[3]
    if (S1 == E1 or E1 == N1 or N1 == D1 or S1 == N1 or S1 == D1 or E1 == D1): continue
    for MORE in range(1000, 9999):
        result2 = digitize(MORE)
        M1 = result2[0]
        O1 = result2[1]
        R1 = result2[2]
        E2 = result2[3]

        if (M1 == O1 or O1 == R1 or R1 == E2 or M1 == O1 or M1 == E2 or M1 == E2 or N1 == R1 or S1 == R1 or E1 == R1 or D1 == R1): continue

        MONEY = SEND + MORE

        result3 = digitize(MONEY)

        M2 = result3[0]
        O2 = result3[1]
        N2 = result3[2]
        E3 = result3[3]
        Y1 = result3[4]

        if (len(str(MONEY)) != 5): continue

        if (M1 != M2 or O1 != O2 or E1 != E2 or E2 != E3 or N1 != N2): continue
            
        if(M2 == O2 or O2 == N2 or N2 == E3 or E2 == Y1 or M2 == N2 or M2 == E3 or M2 == Y1 or O2 == E3 or O2 == Y1 or N2 == Y1 or S1 == R1): continue


        print(f"SEND MORE MONEY: SEND: {SEND} MORE: {MORE} MONEY: {MONEY}")

endtime = time.time()
timediff = endtime - begintime
print(f"{timediff} seconds")