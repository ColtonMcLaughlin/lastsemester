# to run enter python3 python_ETA.py in terminal
import time

def digitize(word):
    length = len(str(word))
    arr = [0,0,0,0,0,0,0,0,0,0]
    rem = word
    count = 0
    for i in range(length - 1, -1, -1):
        divisor = pow(10, i)
        number = (rem) / (divisor)
        rem = rem % divisor

        arr[count] = int(number)
        count+=1
    return arr

# print(time.time())
# begintime, endtime, timediff
begintime = time.time()
for EAT in range(100, 1000):
    result1 = digitize(EAT)
    E1 = result1[0]
    A1 = result1[1]
    T1 = result1[2]

    if (E1 == A1 or A1 == T1 or E1 == T1): continue
        
    for THAT in range(1000, 9999):
        result2 = digitize(THAT)
        T2 = result2[0]
        H1 = result2[1]
        A2 = result2[2]
        T3 = result2[3]

        if (T2 == H1 or H1 == A2 or A2 == T3 or T2 == A2 or H1 == T3): continue

        if (T1 != T2 or T2 != T3 or A1 != A2): continue

        APPLE = EAT + THAT

        result3 = digitize(APPLE)

        A3 = result3[0]
        P1 = result3[1]
        P2 = result3[2]
        L1 = result3[3]
        E2 = result3[4]

        if (len(str(APPLE)) != 5): continue

        if (P1 != P2 or A3 != A1 or E2 != E1): continue

        if (A3 == P1 or P1 == L1 or L1 == E2 or A3 == L1 or A3 == E1): continue

        print(f"EAT THAT APPLE: EAT: {EAT} THAT: {THAT} APPLE: {APPLE}")

endtime = time.time()
timediff = endtime - begintime
print(f"{timediff} seconds")