package eat_that_apple;

// to run go to directory holding file to be compiled, in this case eat_that_apple and cd .. to go back a directory 
// now enter javac eat_that_apple/java_ETA.java to compile
// to run enter java eat_that_apple.java_ETA

class digitize{
    public static int[] main(int word){
        int length = (String.valueOf(word)).length();

        int [] arr = {0,0,0,0,0,0,0,0,0,0};
        int rem = word;
        int number;
        int count = 0;

        int divisor;

        for (int i = length - 1; i >= 0; i--){
            divisor = (int)Math.pow(10, i);
            number = rem / divisor;
            rem = rem % divisor;
            arr[count] = number;
            count++;
        }

        return arr;
    }
}

class java_ETA {
    public static void main(String[] args) {
        int EAT;
        int E1, A1, T1;
    
        int THAT;
        int T2, H1, A2, T3;
    
        int APPLE;
        int A3, P1, P2, L1, E2;

        // long startTime = System.nanoTime();
        // myCall(); 
        // long stopTime = System.nanoTime();
        // System.out.println(stopTime - startTime);
        long begintime = System.nanoTime();
        for (EAT = 100; EAT <= 999; EAT++) {
       
            int [] result1 = digitize.main(EAT);
    
            E1 = result1[0];
            A1 = result1[1];
            T1 = result1[2];
    
            //if e=a=t break out
            if (E1 == A1 || A1 == T1 || E1 == T1) continue;

            for (THAT = 1000; THAT <= 9999; THAT++) {
    
                int [] result2 = digitize.main(THAT);
                T2 = result2[0];
                H1 = result2[1];
                A2 = result2[2];
                T3 = result2[3];
    
                //if t=h=a=t break out
                if (T2 == H1 || H1 == A2 || A2 == T3 || T2 == A2 || H1 == T3) continue;
    
                //if t=t ok
                if (T1 != T2 || T2 != T3 || A1 != A2) continue;
    
                APPLE = EAT + THAT;
    
                int [] result3 = digitize.main(APPLE);
    
                A3 = result3[0];
                P1 = result3[1];
                P2 = result3[2];
                L1 = result3[3];
                E2 = result3[4];
                
                if ((String.valueOf(APPLE)).length() != 5) continue;
                
                if (P1 != P2 || A3 != A1 || E2 != E1) continue;

                if (A3 == P1 || P1 == L1 || L1 == E2 || A3 == L1 || A3 == E1) continue;
    
                System.out.println("EAT THAT APPLE " + "EAT: " + EAT + " THAT: " + THAT + " APPLE: " + APPLE);          
            }
        }
        long endtime = System.nanoTime();
        double timediff = endtime - begintime;
        System.out.println((double)timediff / 1000000000 + " seconds");
    }
} 