//EAT+
//THAT
//=APPLE

//SEND+
//MORE
//=MONEY

//CROSS+
//ROADS
//=DANGER

// To compile go to terminal above and select g++
// To run in terminal enter ./cpp_ETA

#include <iostream>
#include <string>
#include <math.h>
#include <ctime>

using namespace std;

int* digitize(int word) {
    //makes array of length of word
    int length = to_string(word).length();
    static int arr[10];
    int rem = word;
    int number;
    int count = 0;

    for (int i = 0; i < 10; i++) {
        arr[i] = 0;
    }

    int divisor;
    for (int i = length - 1; i >= 0; i--) {
        divisor = pow(10, i);
        number = rem / divisor;
        rem = rem % divisor;

        arr[count] = number;
        count++;
    }
    return arr;
}

int main() {

    int EAT;
    int E1, A1, T1;

    int THAT;
    int T2, H1, A2, T3;

    int APPLE;
    int A3, P1, P2, L1, E2;

    time_t begintime, endtime, timediff;

    begintime = clock();
    for (EAT = 100; EAT <= 999; EAT++) {
       
        int* result1 = digitize(EAT);

        E1 = result1[0];
        A1 = result1[1];
        T1 = result1[2];

        //if e=a=t break out
        if (E1 == A1 || A1 == T1 || E1 == T1) continue;

        for (THAT = 1000; THAT <= 9999; THAT++) {

            int* result2 = digitize(THAT);
            T2 = result2[0];
            H1 = result2[1];
            A2 = result2[2];
            T3 = result2[3];

            //if t=h=a=t break out
            if (T2 == H1 || H1 == A2 || A2 == T3 || T2 == A2 || H1 == T3) continue;

            //if t=t ok
            if (T1 != T2 || T2 != T3 || A1 != A2) continue;

            APPLE = EAT + THAT;

            int* result3 = digitize(APPLE);

            A3 = result3[0];
            P1 = result3[1];
            P2 = result3[2];
            L1 = result3[3];
            E2 = result3[4];

            if (to_string(APPLE).length() != 5) continue;

            if (P1 != P2 || A3 != A1 || E2 != E1) continue;

            if (A3 == P1 || P1 == L1 || L1 == E2 || A3 == L1 || A3 == E1) continue;
            cout << "EAT THAT APPLE: " << "EAT: " << EAT << " THAT: " << THAT << " APPLE: " << APPLE << endl;
        }
    }

    endtime = clock();
    timediff = endtime - begintime;
    cout << (float)timediff / CLOCKS_PER_SEC<< " Seconds" << endl;

    return 0;
}