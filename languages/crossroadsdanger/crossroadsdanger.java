// to compile javac crossroadsdanger.java
// to run java crossroadsdanger
package crossroadsdanger;

class digitize{
    public static int[] main(int word){
        int length = (String.valueOf(word)).length();

        int [] arr = {0,0,0,0,0,0,0,0,0,0};
        int rem = word;
        int number;
        int count = 0;

        int divisor;

        for (int i = length - 1; i >= 0; i--){
            divisor = (int)Math.pow(10, i);
            number = rem / divisor;
            rem = rem % divisor;
            arr[count] = number;
            count++;
        }

        return arr;
    }
}

class crossroadsdanger {
    public static void main(String[] args) {
        int CROSS;
        int C, R, O, S, S2;

        int ROADS;
        int R2, O2, A, D, S3;

        int DANGER;
        int D2, A2, N, G, E, R3;

        long begintime = System.nanoTime();
        for (CROSS = 10000; CROSS <= 99999; CROSS++) {
       
            int [] result1 = digitize.main(CROSS);
    
            C = result1[0];
            R = result1[1];
            O = result1[2];
            S = result1[3];
            S2 = result1[4];

    
            if (C == R || R == O || O == S || C == O || C == S || R == S) continue;
            if (S != S2) continue;

            for (ROADS = 10000; ROADS <= 99999; ROADS++) {
    
                int [] result2 = digitize.main(ROADS);
                R2 = result2[0];
                O2 = result2[1];
                A = result2[2];
                D = result2[3];
                S3 = result2[4];
            
                if (R2 == O2 || O2 == A || A == D || D == S3 || R2 == A || R2 == D || R2 == S3 || R2 == C || O2 == D) continue;
                if (R2 != R || O2 != O || S3 != S2) continue;
                DANGER = CROSS + ROADS;
    
                int [] result3 = digitize.main(DANGER);
    
                D2 = result3[0];
                A2 = result3[1];
                N = result3[2];
                G = result3[3];
                E = result3[4];
                R3 = result3[5];
                
                if ((String.valueOf(DANGER)).length() != 5) continue;
                
                if (A2 != A || R3 != R2 || D2 != D) continue;
            
                if(D2 == A2 || A2 == N || N == G || E == R3 || D2 == N || D2 == G || D2 == E ||
                 D2 == R3 || A2 == G || A2 == E || A2 == R || N == E || N == R3 ||
                 G == E || G == R3 || R3 == N || R == E || N == S3 || G == C || G == O || G == S || N == C || N == R ||
                  N == O || N == S || E == C || E == O || E == S || A2 == S) continue;
    
                System.out.println("CROSS ROADS DANGER " + "CROSS: " + CROSS + " ROADS: " + ROADS + " DANGER: " + DANGER);          
            }
        }
        long endtime = System.nanoTime();
        double timediff = endtime - begintime;
        System.out.println((double)timediff / 1000000000 + " seconds");
    }
} 