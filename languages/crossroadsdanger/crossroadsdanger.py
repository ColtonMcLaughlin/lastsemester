# to run enter python3 python_ETA.py in terminal
import time

def digitize(word):
    length = len(str(word))
    arr = [0,0,0,0,0,0,0,0,0,0]
    rem = word
    count = 0
    for i in range(length - 1, -1, -1):
        divisor = pow(10, i)
        number = (rem) / (divisor)
        rem = rem % divisor

        arr[count] = int(number)
        count+=1
    return arr

# print(time.time())
# begintime, endtime, timediff
begintime = time.time()
for CROSS in range(10000, 100000):
    result1 = digitize(CROSS)
    C = result1[0]
    R = result1[1]
    O = result1[2]
    S = result1[3]
    S2 = result1[4]
    
    if (C == R or R == O or O == S or C == O or C == S or R == S): continue
    if (S != S2): continue
    for ROADS in range(10000, 99999):
        result2 = digitize(ROADS)
        R2 = result2[0]
        O2 = result2[1]
        A = result2[2]
        D = result2[3]
        S3 = result2[4]

        if (R2 == O2 or O2 == A or A == D or D == S3 or R2 == A or R2 == D or R2 == S3 or R2 == C or O2 == D): continue
        if (R2 != R or O2 != O or S3 != S2): continue
        DANGER = CROSS + ROADS

        result3 = digitize(DANGER)

        D2 = result3[0]
        A2 = result3[1]
        N = result3[2]
        G = result3[3]
        E = result3[4]
        R3 = result3[5]

        if (len(str(DANGER)) != 6): continue

        if (A2 != A or R3 != R2 or D2 != D): continue
            
        if(D2 == A2 or A2 == N or N == G or E == R3 or D2 == N or D2 == G or D2 == E or
            D2 == R3 or A2 == G or A2 == E or A2 == R or N == E or N == R3 or
            G == E or G == R3 or R3 == N or R == E or N == S3 or G == C or G == O or G == S or N == C or N == R or
            N == O or N == S or E == C or E == O or E == S or A2 == S): continue

        
        print(f"CROSS ROADS DANGER: CROSS: {CROSS} ROADS: {ROADS} DANGER: {DANGER}")

endtime = time.time()
timediff = endtime - begintime
print(f"{timediff} seconds")