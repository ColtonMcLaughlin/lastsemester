#include <iostream>
#include <string>
#include <regex>
#include <fstream>
#include <vector>

using namespace std;

enum token{
     BeginSym, EndSym, ReadSym, WriteSym, Id, IntLiteral, LParen,
     RParen, SemiColon, Comma, AssignOp, PlusOp, MinusOp, EofSym
};

void printTokens(vector<token> tokens){
    cout << "\n\n";
    for(int i : tokens){
        // cout << i << "\n";
        switch(i){
            case BeginSym: cout << "BeginSym\n"; break;
            case EndSym: cout << "EndSym\n"; break;
            case ReadSym: cout << "ReadSym\n"; break;
            case WriteSym: cout << "WriteSym\n"; break;
            case Id: cout << "Id\n"; break;
            case IntLiteral: cout << "IntLiteral:\n"; break;
            case LParen: cout << "LParen\n"; break;
            case RParen: cout << "RParen\n"; break;
            case SemiColon: cout << "SemiColon\n"; break;
            case Comma: cout << "Comma\n"; break;
            case AssignOp: cout << "AssignOp\n"; break;
            case PlusOp: cout << "PlusOp\n"; break;
            case MinusOp: cout << "MinusOp\n"; break;
            case EofSym: cout << "EofSym\n"; break;
            default: break;
        }
    }
    cout << "\n";
}

bool decideTemp(string temp){
    bool isNum = true;
    
    for(int i = 0; i < temp.length(); i++){
         if(temp[i] != 0 && temp[i] != 1 && temp[i] != 2 && temp[i] != 3 && temp[i] != 4 &&
         temp[i] != 5 && temp[i] != 6 && temp[i] != 7 && temp[i] != 8 && temp[i] != 9){
            isNum = false;
         }
    }
   return isNum;
}

int main()
{
    vector<token> tokens;
    string temp;
    string command;

    ifstream inf("testFile.txt");
    char ch;
    while(inf.get(ch)){

        string str(1,ch);
        regex r("[A-Za-z0-9_]");
        smatch match;

        regex_search(str, match, r);
        
        if (match.size() != 0){
            temp.append(match[0]);
        }else {
                regex r2("[+\n() =:,;]");
                smatch match2;
                regex_search(str, match2, r2);
                if(match2.size() != 0){
                    cout << temp << endl;
                    if((decideTemp(temp) == true) && (str == " ")){
                        cout << "test\n";
                    }else if(decideTemp(temp) == true){
                        // cout << "IntPush\n";
                        tokens.push_back(IntLiteral);
                    }else{
                        // cout << "IDpush\n";
                        if(temp == "BEGIN"){
                            tokens.push_back(BeginSym);
                        }else if(temp == "END"){
                            tokens.push_back(EndSym);
                        }else if(temp == "READ"){
                            tokens.push_back(ReadSym);
                        }else if(temp == "WRITE"){
                            tokens.push_back(WriteSym);
                        }else{
                            tokens.push_back(Id);
                        }
                    }
                    
                    temp.clear();
                    if(str == "=" || str == ":"){
                        command.append(str);
                    }
                }
        }

        switch(ch){
                    case '(' : tokens.push_back(LParen); break;
                    case ')' : tokens.push_back(RParen); break;
                    case ';' : tokens.push_back(SemiColon); break;
                    case ',' : tokens.push_back(Comma); break;
                    case '-' : tokens.push_back(MinusOp); break;
                    case '+' : tokens.push_back(PlusOp); break;
                    case '=' : 
                                if(command == ":="){
                                    tokens.push_back(AssignOp);
                                    command.clear();
                                }
                                break;

                    default: break;
                }

    }

    printTokens(tokens);

    return 0;
}