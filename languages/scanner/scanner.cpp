#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

// token types
// BeginSym, EndSym, ReadSym, WriteSym, Id, IntLiteral, LParen,
// RParen, SemiColon, Comma, AssignOp, PlusOp, MinusOp, EofSym

using namespace std;


enum token{
     BeginSym, EndSym, ReadSym, WriteSym, Id, IntLiteral, LParen,
     RParen, SemiColon, Comma, AssignOp, PlusOp, MinusOp, EofSym
};

void printTokens(vector<token> tokens, vector<int> nums){
    cout << "\n\n";
    for(int i : tokens){
        // cout << i << "\n";
        switch(i){
            case BeginSym: cout << "BeginSym\n"; break;
            case EndSym: cout << "EndSym\n"; break;
            case ReadSym: cout << "ReadSym\n"; break;
            case WriteSym: cout << "WriteSym\n"; break;
            case Id: cout << "Id\n"; break;
            case IntLiteral: cout << "IntLiteral: " << nums.front() << endl; nums.erase(nums.begin()); break;
            case LParen: cout << "LParen\n"; break;
            case RParen: cout << "RParen\n"; break;
            case SemiColon: cout << "SemiColon\n"; break;
            case Comma: cout << "Comma\n"; break;
            case AssignOp: cout << "AssignOp\n"; break;
            case PlusOp: cout << "PlusOp\n"; break;
            case MinusOp: cout << "MinusOp\n"; break;
            case EofSym: cout << "EofSym\n"; break;
            default: break;
        }
    }
    cout << "\n";
}

int main(){
    vector<token> tokens;
    vector<int> nums;
    vector<string> ids;
    vector<string> temp;

    cout << "Scanning...\n";

    ifstream inf("testFile.txt");

    char ch;
    string id;
    string command;
    while(inf.get(ch)){
        // cout << ch;

        switch(ch){
            case 'A': command.append("A");
                      temp.push_back("A");
                      break;
            case 'B': command.append("B");
                      temp.push_back("A"); 
                      break;  
            case 'D': command.append("D");
                      temp.push_back("A");
                      if(command == "READ"){
                          tokens.push_back(ReadSym);
                          command.clear();
                      }else if (command == "END"){
                          tokens.push_back(EndSym);
                          command.clear();
                      }
                      break;
            case 'E': command.append("E");
                      if(command == "WRITE"){
                          tokens.push_back(WriteSym);
                      }
                      break;  
            case 'G': command.append("G"); 
                      break;  
            case 'I': command.append("I"); 
                      break;  
            case 'N': command.append("N");
                      if(command == "BEGIN"){
                          tokens.push_back(BeginSym);
                          command.clear();
                      } 
                      break;  
            case 'T': command.append("T");
                      break;
            case 'R': command.append("R");
                      break;
            case 'W': command.append("W");
                      break;
            case '0': command.clear();
                      tokens.push_back(IntLiteral);
                      nums.push_back(0);
                      break;
            case '1': command.clear();
                      tokens.push_back(IntLiteral);
                      nums.push_back(1);
                      break;
            case '2': command.clear();
                      tokens.push_back(IntLiteral);
                      nums.push_back(2);
                      break;
            case '3': command.clear();
                      tokens.push_back(IntLiteral);
                      nums.push_back(3);
                      break;          
            case '4': command.clear();
                      tokens.push_back(IntLiteral);
                      nums.push_back(4);
                      break;          
            case '5': command.clear();
                      tokens.push_back(IntLiteral);
                      nums.push_back(5);
                      break;          
            case '6': command.clear();
                      tokens.push_back(IntLiteral);
                      nums.push_back(6);
                      break;          
            case '7': command.clear();
                      tokens.push_back(IntLiteral);
                      nums.push_back(7);
                      break;          
            case '8': command.clear();
                      tokens.push_back(IntLiteral);
                      nums.push_back(8);
                      break;          
            case '9': command.clear();
                      tokens.push_back(IntLiteral);
                      nums.push_back(9);
                      break;          
            case '{': command.clear(); break;
            case '}': command.clear(); break;
            case '(': command.clear();
                      tokens.push_back(LParen);
                      break;
            case ')': command.clear();
                      tokens.push_back(RParen);
                      break;
            case ';': tokens.push_back(SemiColon); 
                      command.clear();
                      break;
            case ':': command.append(":");
                      break;
            case '=': command.append("=");
                      if(command == ":="){
                          tokens.push_back(AssignOp);
                      }
                      command.clear();
                      break;
            case ',': tokens.push_back(Comma); 
                      command.clear();
                      break;
            case ' ': command.clear(); break;
            case '-': tokens.push_back(MinusOp);
                      command.clear(); 
                      break;
            case '+': tokens.push_back(PlusOp);
                      command.clear();
                      break;
            case '\n': command.clear(); break;
            default: break;
        }
        // cout << command << endl;
        // for(int i : nums){
        //     cout << i;
        // }
        // cout << endl;
    }

    printTokens(tokens, nums);

    return 0;
}