-- import for showing data types
import Data.Dynamic

-- types in Haskell: 

-- Bool
-- Int (bounded)
-- Integer (unbounded- as long as computer is capable of handling)
-- Float 
-- Double
-- Char
-- [Char] (list of chars- basically a string)
-- Tuple (list of items- not required to be of same type)

main = do
    -- basics ------------------------------------------------------------

    --declare variable with let
    -- specify type with ":: typehere"
    let num = 12 :: Int

    --print out type of num
    print(dynTypeRep(toDyn(num)))

    -- Bool --------------------------------------------------------------

    let a = True :: Bool
    let b = False :: Bool

    putStrLn ("a && b")
    print(a && b)
    putStrLn("\n")

    putStrLn ("a || b")
    print(a || b)
    
    -- Int ---------------------------------------------------------------
    
    let maxInt = maxBound :: Int
    putStrLn("Max size of Int is: ")
    print(maxInt)
    
    -- Char/ [Char] --------------------------------------------------------------

    -- no types declared
    let aCharacter = 'a'
    let bCharacter = "b"
    let state = "Utah"

    putStrLn("\n")
    print(aCharacter)
    print(bCharacter)
    print(state)

    -- haskell can find type without declaration but it is best practice to declare type
    print(dynTypeRep(toDyn(aCharacter)))
    print(dynTypeRep(toDyn(bCharacter)))
    print(dynTypeRep(toDyn(state)))