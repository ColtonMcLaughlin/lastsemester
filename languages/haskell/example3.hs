-- declare variable of type int
demoFunction :: Int -> Int
demoFunction x = x * x


main = do 
let argument = 4 :: Int
print(demoFunction argument)

let returnedValue = (demoFunction argument)

putStrLn("returnedValue is " ++ show returnedValue)

