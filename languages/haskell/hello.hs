-- install with this commmand: sudo apt-get install haskell-platform
-- Then install Haskell Syntax Highlighting extension on vs code

main = putStrLn "Hello, World!"

-- Compiling Files
--  ghc (Glasgow Haskell Compilation system) GHC is the standard Haskell compiler
-- -o (output file)

-- to compile enter ghc fileToBeCompiled -o target

-- so in this case enter: ghc hello.hs -o hello
-- to run enter: ./hello