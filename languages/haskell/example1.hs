-- declare variable of type int
x :: Int
x = 3

-- this wont work
-- x = 4

main = do 
    -- print variable
    print x

    let var = x
    if var `rem` 2 == 0
        then putStrLn (show var ++ " is Even")
    else putStrLn (show var ++ " is Odd")