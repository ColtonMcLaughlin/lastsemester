-- Compute the sum if the integers from 1 to n.
-- sumtorial :: Integer -> Integer
-- sumtorial 0 = 0
-- sumtorial n = n + sumtorial (n - 1)

-- main = do
--     print(sumtorial 1)
--     print(sumtorial 2)
--     print(sumtorial 3)
--     print(sumtorial 4)
---------------------------------------------------------------------------------------------------------

add :: Integer -> Integer -> Integer   --function declaration 
add x y =  x + y                       --function definition 

main = do 
    putStrLn "The addition of the two numbers is:"  
    print(add 2 5)    --calling a function 
    
    let value = (add 2 5)

    print(value * 2)
